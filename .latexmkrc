# -synctex=1 to jump between pdf and text editor. Follows clicks
# -interaction=nonstopmode will not stop at missing file
#  reference and ask for alternatives
$pdflatex = "xelatex -synctex=1 -interaction=nonstopmode %O %S";
$pdf_mode = 1;
$dvi_mode = 0;
